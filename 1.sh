#!/bin/bash

rs_path="$HOME/.config/rekcuF_Sniffer"

rs_url_file="https://raw.githubusercontent.com/opendns/public-domain-lists/master/opendns-top-domains.txt"

rs_concurrent=${1:-"3"}

rs_timeout=${2:-"4"}

rs_ver="rekcuF Sniffer 1.1"

rs_handler() {
    rs_url=${1:-""}
    rs_timeout=${2:-"4"}
    if [ -n "$rs_url" ];then
        rs_url_full="https://$rs_url/"
        rs_result=`curl -k -s -X GET -o /dev/null -w "%{http_code}" -m $rs_timeout "$rs_url_full"`
        rs_result_stat=${rs_result//[[:space:]]/}
        printf " %-44s > %-3s\n" "$rs_url" "$rs_result_stat"
    fi
}

chr_repeat() {
    for ((i=0;i<${2:-"16"};i++));do echo -n ${1:-"="};done
}

export -f rs_handler
export rs_timeout

if [ $rs_concurrent = "--help" ];then
    printf "\nUsage:\n\trs1 [1] [2]\n\n\n1 : How many http request same time\n2 : How much wait for website response\n\n"
    exit
fi

[ ! -d "$rs_path" ] && mkdir -p "$rs_path"

rs_file="${rs_path}/db.txt"

rs_file_size=0
[ -f "$rs_file" ] && rs_file_size=`stat -c%s "$rs_file"`

if [ $rs_file_size -lt 9 ];then
    rm -f "$rs_file" >/dev/null 2>&1
    if command -v wget >/dev/null 2>&1;then
        wget --no-check-certificate -q --show-progress -O "$rs_file" "$rs_url_file"
    else
        curl -o "$rs_file" "$rs_url_file"
    fi
    printf "\n\n< $rs_url_file"
    printf "\n\n> $rs_file"
fi

printf "\n\n"; chr_repeat "-" 16; echo -n " $rs_ver "; chr_repeat "-" 16; printf "\n\n"
printf " @ Concurrent = $rs_concurrent\n"
printf " @ Timeout = $rs_timeout\n\n"
chr_repeat "-" 52
printf "\n\n"

while true;do
    shuf $rs_file |xargs -P $rs_concurrent -I {} sh -c 'rs_handler "{}" $rs_timeout'
done
